using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using _4._5_Tarefas.Utill;
using _4._5_Tarefas.ViewModels;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media.Imaging;
using Avalonia.Threading;

namespace _4._5_Tarefas.Views;

/// <summary>
/// Classe principal que representa a janela do jogo.
/// </summary>
public partial class MainWindow : Window
{
    /// <summary>
    /// Representa o elemento de tela do jogador.
    /// </summary>
    public Canvas? Gamer;
    private int _countFruid;
    private const double EnemyWidth = 40;
    private const double EnemyHeight = 40;
    private readonly MainWindowViewModel _viewModel;
    private List<Image> _fruits;
    private Sound _sound;
    private int Point;

    private Bitmap _boll =
        new Bitmap("C:\\Users\\Jônatas\\RiderProjects\\4.5 Tarefas\\Lab_4\\Lab_4\\Lab-4\\Assets\\Images\\Boll Blue.gif");

    private Bitmap _bollEat =
        new Bitmap("C:\\Users\\Jônatas\\RiderProjects\\4.5 Tarefas\\Lab_4\\Lab_4\\Lab-4\\Assets\\Images\\Boll Blue eat.gif");

    private Image? _play;
    private bool _isStart = true;
    private int _score;
    private double _eixoX;
    private double _eixoY;

    private readonly Dictionary<int, string?> _fruitPaths = new Dictionary<int, string?>
    {
        { 0, "Abacaxi.png" },
        { 1, "Banana.png" },
        { 2, "Fruta.png" },
        { 3, "Laranja.png" },
        { 4, "Maça.png" },
        { 5, "Uva.png" },
    };


    /// <summary>
    /// Construtor da classe MainWindow.
    /// </summary>
    public MainWindow()
    {
        InitializeComponent();
        Gamer = this.FindControl<Canvas>("JogoDeFruta");
        _play = this.FindControl<Image>("Play");
        _fruits = new List<Image>();
        _sound = new Sound();
        Score.Text = $"Seu Score: {Point}";
        _viewModel = new MainWindowViewModel();

        if (_isStart) SpawnFruitWave();

        DataContext = _viewModel;
    }

    /// <summary>
    /// Manipula eventos de teclado para permitir movimento do jogador.
    /// </summary>
    protected override void OnKeyDown(KeyEventArgs e)
    {
        base.OnKeyDown(e);
        switch (e.Key)
        {
            case Key.Left:
                MoveLeft();
                break;
            case Key.Right:
                MoveRight();
                break;
            case Key.Up:
                MoveUp();
                break;
            case Key.Down:
                MoveDawn();
                break;
        }
    }

    /// <summary>
    /// Gera uma onda inicial de frutas.
    /// </summary>
    private void SpawnFruitWave()
    {
        _isStart = false;
        for (int i = 0; i < 7; i++)
        {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            SetFruit();
            _countFruid++;
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        }
    }

    /// <summary>
    /// Configura uma nova fruta na tela.
    /// </summary>
    private void SetFruit()
    {
        Random random = new Random();
        int row = random.Next(6);
        Bitmap source;


        string? fileName;

        if (_fruitPaths.TryGetValue(row, out fileName))
        {
            string fullPath =
                Path.Combine("C:\\Users\\Jônatas\\RiderProjects\\4.5 Tarefas\\Lab_4\\Lab_4\\Lab-4\\Assets\\Images\\Fruit",
                    fileName);
            source = new Bitmap(fullPath);
        }
        else
        {
            throw new InvalidOperationException("Valor de row não mapeado para um caminho de imagem.");
        }

        var enemyIo = new Image
        {
            Width = EnemyWidth,
            Height = EnemyHeight,
            Source = source
        };


        Canvas.SetLeft(enemyIo, random.Next(350));
        Canvas.SetTop(enemyIo, random.Next(300));

        Dispatcher.UIThread.Post(() =>
        {
            Gamer?.Children.Add(enemyIo);
            _fruits.Add(enemyIo);
        });
    }
    /// <summary>
    /// Move o jogador para a esquerda.
    /// </summary>
    public void MoveLeft()
    {
        _eixoX -= 10;
        if (_play != null) Canvas.SetLeft(_play, _eixoX);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        CheckCollision();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }
    /// <summary>
    /// Move o jogador para a direita.
    /// </summary>
    public void MoveRight()
    {
        _eixoX += 10;
        if (_play != null) Canvas.SetLeft(_play, _eixoX);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        CheckCollision();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }
    /// <summary>
    /// Move o jogador para a cima.
    /// </summary>
    public void MoveUp()
    {
        _eixoY -= 10;
        if (_play != null) Canvas.SetTop(_play, _eixoY);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        CheckCollision();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    /// <summary>
    /// Move o jogador para a baixo.
    /// </summary>
    public void MoveDawn()
    {
        _eixoY += 10;
        if (_play != null) Canvas.SetTop(_play, _eixoY);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
        CheckCollision();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
    }

    /// <summary>
    /// Verifica se houve colisão entre o jogador e as frutas.
    /// </summary>
    private async Task CheckCollision()
    {
        Rect rectBullet = new Rect(Canvas.GetLeft(Play), Canvas.GetTop(Play), Play.Width, Play.Height);

        List<Image> fruitsCopy = new List<Image>(_fruits);

        foreach (Image fruit in fruitsCopy)
        {
            Rect rectEnemy = new Rect(Canvas.GetLeft(fruit), Canvas.GetTop(fruit), fruit.Width, fruit.Height);

            if (rectBullet.Intersects(rectEnemy))
            {
                _fruits.Remove(fruit);

                if (_play != null)
                {
                    _play.Source = _bollEat;

                    await RemoveFruit(fruit);
                    await Task.Delay(500);
                    _play.Source = _boll;
                }
            }
        }
    }

    /// <summary>
    /// Remove uma fruta da tela e realiza ações associadas.
    /// </summary>
    private Task RemoveFruit(Image fruit)
    {
        if (_countFruid == 0)
        {
            SpawnFruitWave();
        }
        
        _countFruid--;


        Point += 10;
        Score.Text = $"Seu Score: {Point}";
        
        _sound.PlaySoundEat();
        
        if (Gamer != null) Gamer.Children.Remove(fruit);
        
        return Task.CompletedTask;
    }

}