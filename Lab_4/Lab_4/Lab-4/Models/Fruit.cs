﻿using Avalonia.Media.Imaging;

namespace _4._5_Tarefas.Models;

public class Fruit(int points, Bitmap Source)
{
    public Bitmap Source { get; } = Source;
    public int Points { get; } = points;
}