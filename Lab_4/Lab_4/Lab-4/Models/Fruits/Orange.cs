﻿using Avalonia.Media.Imaging;

namespace _4._5_Tarefas.Models.Fruits;

public class Orange(int points, Bitmap Source)
{
    public Bitmap Source { get; } = Source;
    public int Points { get; } = points;
}