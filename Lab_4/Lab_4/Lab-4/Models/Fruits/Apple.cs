﻿using Avalonia.Media.Imaging;

namespace _4._5_Tarefas.Models.Fruits;

public class Apple
{
    public Bitmap Source { get; } =
        new Bitmap("C:\\Users\\Jônatas\\RiderProjects\\4.5 Tarefas\\4.5 Tarefas\\Assets\\Images\\Fruit\\Maça.png");

    public int Points { get; } = 10;
}