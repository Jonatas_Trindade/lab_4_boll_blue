﻿using System;
using System.IO;
using System.Threading.Tasks;
using NAudio.Wave;

namespace _4._5_Tarefas.Models;


public class SoundPlayer
{
    private WaveOutEvent waveOutEvent;
    private AudioFileReader audioFileReader;

    public SoundPlayer(string filePath)
    {
        waveOutEvent = new WaveOutEvent();
        audioFileReader = new AudioFileReader(filePath);
        waveOutEvent.Init(audioFileReader);
    }

    public void Play()
    {
        waveOutEvent.Stop();
        waveOutEvent.Play();
    }

    public void Stop()
    {
        waveOutEvent.Stop();
    }
    public void Dispose()
    {
        waveOutEvent?.Dispose();
        audioFileReader?.Dispose();
    }
}