﻿using System.Diagnostics;
using System.Media;

namespace _4._5_Tarefas.Utill
{
    /// <summary>
    /// Classe responsável por gerenciar a reprodução de sons no jogo.
    /// </summary>
    public class Sound
    {
        /// <summary>
        /// Reproduz o som associado à ação de comer (Eat) no jogo.
        /// </summary>
        public void PlaySoundEat()
        {
            // Cria uma instância do SoundPlayer.
            SoundPlayer soundPlayer = new SoundPlayer();

            // Verifica se a instância do SoundPlayer foi criada com sucesso.
            Debug.Assert(soundPlayer != null, nameof(soundPlayer) + " != null");

            // Define a localização do arquivo de som associado à ação de comer.
            soundPlayer.SoundLocation = @"C:\Users\Jônatas\RiderProjects\4.5 Tarefas\Lab_4\Lab_4\Lab-4\Assets\Sons\Eating.wav";

            // Reproduz o som.
            soundPlayer.Play();
        }
    }
}